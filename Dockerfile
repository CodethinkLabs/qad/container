FROM ubuntu:22.04

# Install dependencies and tools.
RUN apt update && \
    apt install -y \
    build-essential \
    clang-format \
    clang-tidy \
    cmake \
    g++-10 \
    gcc-10 \
    git \
    libdrm-dev \
    libgles-dev \
    libpng-dev \
    libwayland-dev \
    libweston-9-dev \
    make \
    meson \
    ninja-build \
    patch \
    pkg-config \
    weston \
    wget

RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 10 && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-10 10

ARG LIBMICROHTTPD_VERSION=0.9.77
RUN wget https://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-${LIBMICROHTTPD_VERSION}.tar.gz && \
    tar xf libmicrohttpd-${LIBMICROHTTPD_VERSION}.tar.gz && \
    cd libmicrohttpd-${LIBMICROHTTPD_VERSION} && \
    ./configure --enable-static --disable-https --prefix=/usr && \
    make install

# Build wayland-ivi-extension to get ilm dependencies.
RUN git clone https://github.com/GENIVI/wayland-ivi-extension.git && cd wayland-ivi-extension && \
    git checkout 2.3.0 && \
    mkdir build && cd build && cmake .. && make install
